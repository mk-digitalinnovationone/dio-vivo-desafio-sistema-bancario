# DIO - VIVO Python AI - Desafio -> Sistema Bancário

Este é um sistema bancário simples implementado em Python que permite ao usuário realizar operações básicas 
como depósito, saque, verificação de extrato e saída do sistema.

## Funcionalidades

- **Depositar**: Permite ao usuário adicionar fundos à conta.
- **Sacar**: Permite ao usuário retirar fundos da conta, respeitando um limite de saque e um número 
    máximo de saques diários.
- **Extrato**: Exibe o histórico de transações e o saldo atual.
- **Sair**: Encerra a execução do programa.

## Estrutura do Menu

O menu é apresentado em um loop `while` que permanece ativo até que o usuário escolha a opção de sair (`q`). As opções disponíveis são:

- `[d] Depositar`
- `[s] Sacar`
- `[e] Extrato`
- `[q] Sair`

## Como Usar

1. **Depositar**: Para realizar um depósito, digite `d` e informe o valor do depósito. O valor deve ser maior que 0.
2. **Sacar**: Para realizar um saque, digite `s` e informe o valor do saque. O saque será realizado apenas se:
   - O saldo for suficiente.
   - O valor do saque não exceder o limite.
   - O número de saques diários não exceder o limite permitido.
3. **Extrato**: Para verificar o extrato, digite `e`. O sistema exibirá todas as transações realizadas e o saldo atual.
4. **Sair**: Para encerrar o programa, digite `q`.

## Exemplo de Execução

```bash
[d] Depositar
[s] Sacar
[e] Extrato
[q] Sair

=> d
Informe o valor do depósito: 100
[d] Depositar
[s] Sacar
[e] Extrato
[q] Sair

=> s
Informe o valor do saque: 50
[d] Depositar
[s] Sacar
[e] Extrato
[q] Sair

=> e

================ EXTRATO ================
Depósito: R$ 100.00
Saque: R$ 50.00

Saldo: R$ 50.00
==========================================

[d] Depositar
[s] Sacar
[e] Extrato
[q] Sair

=> q
```
